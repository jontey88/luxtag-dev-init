import nem from 'nem-sdk';

import store from '../store';
import { parseAccountData, parseTransactionData, addUnconfirmedTransaction, addConfirmedTransaction } from './index';

let connector = null;

export function connect() {
  connector = nem.com.websockets.connector.create(store.state.socket, store.state.account.address);
  connector.connect().then(() => {
    store.commit('updateConnected', true);

    nem.com.websockets.subscribe.account.data(connector, (res) => {
      parseAccountData(res);
    });

    // Subscribe to recent transactions channel
    nem.com.websockets.subscribe.account.transactions.recent(connector, (res) => {
      parseTransactionData(res);
    });
    // Subscribe to unconfirmed transactions channel
    nem.com.websockets.subscribe.account.transactions.unconfirmed(connector, (res) => {
      addUnconfirmedTransaction(res);
    });
    // Subscribe to unconfirmed transactions channel
    nem.com.websockets.subscribe.account.transactions.confirmed(connector, (res) => {
      addConfirmedTransaction(res);
    });
    nem.com.websockets.requests.account.data(connector);
    nem.com.websockets.requests.account.transactions.recent(connector);
  }, (err) => {
    // console.log(err);
  });
}

export function disconnect() {
  store.commit('updateConnected', false);
  connector.close();
  connector = null;
}
