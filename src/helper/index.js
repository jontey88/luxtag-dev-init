import store from '../store';

const balanceComma = new Intl.NumberFormat('en-US');

export function formatBalance(balance) {
  return `${balanceComma.format(balance / 1000000)} XEM`;
}

export function parseAccountData(res) {
  store.dispatch('updateBalance', formatBalance(res.account.balance));
}

export function parseTransactionData(res) {
  store.dispatch('updateTransactions', res.data);
}

export function addUnconfirmedTransaction(res) {
  store.dispatch('unconfirmedTransaction', res);
}

export function addConfirmedTransaction(res) {
  store.dispatch('confirmedTransaction', res);
}
