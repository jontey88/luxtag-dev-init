import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({
  state: {
    endpoint: {
      host: 'https://adamexperimental.cyberblox.my',
      port: 7891,
    },
    socket: {
      host: 'https://adamexperimental.cyberblox.my',
      port: 7779,
    },
    connected: false,
    account: {
      address: '',
      privateKey: '',
      balance: null,
    },
    transactions: [],
    lastTransactionHash: null,
  },
  getters: {
    endpoint: state => state.endpoint,
    socket: state => state.socket,
    connected: state => state.connected,
    address: state => state.account.address,
    balance: state => state.account.balance,
    transactions: state => state.transactions,
  },
  mutations: {
    updateAddress(state, payload) {
      state.account.address = payload;
    },
    updateBalance(state, payload) {
      state.account.balance = payload;
    },
    updateTransactions(state, payload) {
      state.transactions = payload;
    },
    updateLastTransactionHash(state, payload) {
      state.lastTransactionHash = payload;
    },
    unconfirmedTransaction(state, payload) {
      // Sometimes socket triggers twice. Check if transaction is already stored
      if (state.transactions.findIndex(t => t.meta.hash.data === payload.meta.hash.data) > 0) {
        return;
      }
      const transaction = payload;
      transaction.meta.id = 'Unconfirmed';
      state.transactions.unshift(transaction);
    },
    confirmedTransaction(state, payload) {
      // Find transaction with same hash
      // Remove it and add the new payload
      const i = state.transactions.findIndex(t => t.meta.hash.data === payload.meta.hash.data);
      state.transactions.splice(i, 1);
      const transaction = payload;
      state.transactions.unshift(transaction);
    },
    updateConnected(state, payload) {
      state.connected = payload;
    },
  },
  actions: {
    updateAddress({ commit }, payload) {
      commit('updateAddress', payload);
    },
    updateBalance({ commit }, payload) {
      commit('updateBalance', payload);
    },
    updateTransactions({ commit }, payload) {
      commit('updateTransactions', payload);
    },
    unconfirmedTransaction({ commit }, payload) {
      commit('unconfirmedTransaction', payload);
    },
    confirmedTransaction({ commit }, payload) {
      commit('confirmedTransaction', payload);
    },
    updateConnected({ commit, state }, payload) {
      commit('updateConnected', payload);
    },
  },
  strict: debug,
});
